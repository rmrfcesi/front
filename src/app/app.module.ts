import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';

import { LayoutModule } from '@angular/cdk/layout';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ChatComponent } from './GAME/chat/chat.component';
import { ContactsComponent } from './GAME/chat/contacts/contacts.component';
import { MessagesComponent } from './GAME/chat/messages/messages.component';
import { ShowMessageComponent } from './GAME/chat/messages/show_message/show-message/show-message.component';
import { FaqComponent } from './pages/faq/faq.component';
import { IstComponent } from './pages/ist/ist.component';
import { IstsComponent } from './pages/ists/ists.component';
import { TemoignagesComponent } from './pages/temoignages/temoignages.component';
import { ChoiceComponent } from './GAME/chat/choices/choice/choice.component';
import { TemoignageComponent } from './pages/temoignage/temoignage.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    ChatComponent,
    MessagesComponent,
    IstsComponent,
    IstComponent,
    FaqComponent,
    HeaderComponent,
    ShowMessageComponent,
    TemoignagesComponent,
    ChoiceComponent,
    TemoignageComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatGridListModule,
    LayoutModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
