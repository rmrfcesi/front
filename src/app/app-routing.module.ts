import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsComponent } from './GAME/chat/contacts/contacts.component';
import { MessagesComponent } from './GAME/chat/messages/messages.component';
import { FaqComponent } from './pages/faq/faq.component';
import { IstComponent } from './pages/ist/ist.component';
import { IstsComponent } from './pages/ists/ists.component';
import { TemoignagesComponent } from './pages/temoignages/temoignages.component';
import { TemoignageComponent } from './pages/temoignage/temoignage.component';

const routes: Routes = [
  { path: '', component: ContactsComponent },
  { path: 'message/:id', component: MessagesComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'temoignages', component: TemoignagesComponent },
  { path: 'temoignage', component: TemoignageComponent },
  { path: 'ist/:name', component: IstComponent },
  { path: 'ists', component: IstsComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
