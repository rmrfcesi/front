import { Component, OnInit } from '@angular/core';
import { temoignages } from 'src/app/mocks/temoignages';

@Component({
  selector: 'app-temoignages',
  templateUrl: './temoignages.component.html',
  styleUrls: ['./temoignages.component.scss']
})
export class TemoignagesComponent implements OnInit {

  constructor() { }
  temoignages = temoignages
  ngOnInit(): void {
  }

}
