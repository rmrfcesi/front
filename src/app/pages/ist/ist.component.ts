import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ists } from 'src/app/mocks/ists';

@Component({
  selector: 'app-ist',
  templateUrl: './ist.component.html',
  styleUrls: ['./ist.component.scss'],
})
export class IstComponent implements OnInit {
  constructor(private route: ActivatedRoute) {}
  ist: {
    name: string;
    titre: string;
    description: string;
  } | null = null;
  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.ist = ists.filter((ist) => ist.name === params.get('name'))[0];
    });
  }
}
