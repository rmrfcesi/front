import { Component, OnInit } from '@angular/core';
import { randomInt } from 'crypto';

@Component({
  selector: 'app-temoignage',
  templateUrl: './temoignage.component.html',
  styleUrls: ['./temoignage.component.scss']
})
export class TemoignageComponent implements OnInit {
  temoignage = "";
  left = "";
  top = "";
  constructor() { 

  }
  addTemoignage():void {
    console.log(this.temoignage);
    
  }
  onChange(txt: any):void {
    let elem = document.getElementById("btn");
    if (elem != null && txt !== "") {
      elem.style.left = this.left;
      elem.style.top = this.top;
    }
  }


  mouseenter():void {
    let elem = document.getElementById("btn");
    if(elem != null && this.temoignage === "") 
    {    
      elem.style.left =  this.randomInteger(0, innerWidth) + "px";
      elem.style.top = this.randomInteger(0, innerHeight) + "px";
    }else if (elem != null && this.temoignage !== "") {
      elem.style.left = this.left;
      elem.style.top = this.top;
    }
  }
  ngOnInit(): void {
    const elem=document.getElementById("btn")
    if(elem)this.left = elem.style.left;
    if(elem)this.top = elem.style.top;

    
  }

  randomInteger(min:number, max:number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
