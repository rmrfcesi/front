export const ists = [
  {
    name: 'syphilis',
    titre: 'Syphilis',
    short:
      "<div><p>Une bactérie, le tréponème pâle, est à l'origine de la <a >syphilis</a>.</p><p>Entre 2013 et 2015, le nombre de <strong>syphilis récentes</strong> a été en augmentation. En 2016, le nombre de cas de syphilis récentes diagnostiqués reste élevé mais n’augmente pas par rapport à 2015, quelle que soit l’orientation sexuelle. Les hommes ayant des rapports sexuels avec les hommes représentent 81<span>&nbsp;</span>% des cas rapportés en 2016.</p><p>2<span>&nbsp;</span>500 cas de syphilis ont été diagnostiqués en CeGIDD en 2020, avec une diminution du nombre de diagnostics de 18<span>&nbsp;</span>% entre 2019 et 2020. Les hommes ayant des rapports sexuels avec les hommes représentent 61<span>&nbsp;</span>% de ces cas.</p></div>",
    description:
      '<div>Une succession de 3 stades cliniques caractérise l’évolution de la syphilis chez les personnes non traitées. Les symptômes sont propres à chacune de ces phases (chancre, roséole, syphilides...) et peuvent parfois être difficiles à détecter. Le diagnostic repose sur des tests sérologiques.</div>',
  },
  {
    name: 'hepatite',
    titre: 'Hépatite B ',
    short:
      "<div><div><p>Provoquée par un virus très contagieux (VHB), <a>l'hépatite B</a> atteint essentiellement le foie. Dans&nbsp; 2 à 10<span>&nbsp;</span>% des cas, l'hépatite B devient chronique. La maladie est longtemps silencieuse, mais la personne porteuse du virus peut le transmettre.</p><p>On estime que 135 700 personnes sont porteuses d'une hépatite B chronique en France métropolitaine en 2016.</p></div></div>",
    description:
      '<div >L’hépatite B est une maladie virale du foie due au VHB. Cette maladie se transmet facilement par voie sexuelle et par le sang. Elle peut être prévenue par la vaccination.</div>',
  },
  {
    name: 'chlamydioses',
    titre: 'Chlamydioses',
    short:
      "<div><p>Les<strong> </strong><a  >chlamydioses</a> sont des <strong>IST</strong> dues à une bactérie (<strong>Chlamydia trachomatis</strong>), susceptible de provoquer une infection uro-génitale ou une infection ano-rectale chez l'homme.</p><p><strong>Entre 2015 et 2017</strong></p><p>Le nombre d’<strong>infections uro-génitales</strong> à <strong>Chlamydia trachomatis</strong> a augmenté de 16<span>&nbsp;</span>%. 60<span>&nbsp;</span>% des cas rapportés en 2017 concernent des femmes, en majorité âgées de 15 à 24<span>&nbsp;</span>ans.<br>Le&nbsp;nombre d’<strong>infections ano-rectales</strong> à <strong>Chlamydia trachomatis</strong> (lymphogranulomatoses vénériennes rectales) est en augmentation en 2016. En 2017, plus de 90<span>&nbsp;</span>% des cas concernent des hommes ayant des rapports sexuels avec des hommes.</p><p><strong>Entre 2019 et 2020 </strong></p><p>Le nombre de diagnostics d’infection à Chlamydia a diminué de 8<span>&nbsp;</span>% dans les laboratoires privés et de 31<span>&nbsp;</span>% en CeGIDD. Il est probable que la <span ><button >pandémie</button></span> à SARS-CoV-2 explique la baisse du recours au dépistage.</p><p>70<span>&nbsp;</span>% des diagnostics faits dans le secteur privé a concerné des femmes et 90<span>&nbsp;</span>% des lymphogranulomatoses vénériennes rectales (LGV) ont été diagnostiquées chez les hommes ayant des relations sexuelles avec des hommes.</p></div>",
    description:
      '<div >L’infection sexuellement transmissible à Chlamydia est due à une bactérie. Elle est transmise notamment lors de rapports sexuels non protégés. Elle survient à tout âge mais touche plus particulièrement les jeunes femmes.</div>',
  },
];
