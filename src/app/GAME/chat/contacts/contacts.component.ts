import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';
import { Chat } from '../model';
import { chats } from '../../../../assets/scenario/scenario';
@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss'],
})
export class ContactsComponent implements OnInit {
  public chats: Chat[] = [];

  constructor(public chatService: ChatService) {}

  ngOnInit(): void {
    this.chatService.newChat.subscribe((chat) => {
      this.newChat(chat);
    });
    this.chatService.addChat(this.chatService.chatMap.get(1)!);
  }

  private newChat(chat: Chat): void {
    this.chats.push(chat);
    console.log('chaaaat', this.chats);
  }

  public getLastMessageContact(chat: Chat): any {
    return this.chatService.contactMap.get(
      chat.getMessages()[chat.getMessages().length - 1].getContactId()
    );
  }

  public getLastMessage(chat: Chat): any {
    return chat.getMessages()[chat.getMessages().length - 1].getText();
  }
}

export interface Message {
  id_contact: number;
  last_message: string;
}
