import { Component, Input, OnInit } from '@angular/core';
import { Chat, Message } from '../../../model';
@Component({
  selector: 'app-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.scss']
})
export class ShowMessageComponent implements OnInit {

  @Input()
  public chat? : Chat
  
  public messages: Message[] = [];

  constructor() { }

  ngOnInit(): void {
    
    this.messages = this.chat?.getMessages()!;

  }

}
