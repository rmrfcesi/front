import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ChatService } from '../chat.service';
import { Chat, Choice, Message } from '../model';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  
  public chatId?: Number;

  public chat!: Chat;

  public choices?: Choice[];

  header = "Chat avec ";
  public contactName = "";
  public numberMessagesWritten = 0;
  constructor(private route: ActivatedRoute, private chatService : ChatService) {


  }

  ngOnInit(): void {
  
    this.route.paramMap.subscribe((params: ParamMap) => {
      let idString = params.get('id');
      this.chatId = parseInt(idString!)
    });
    this.chat = this.chatService.chatMap.get(this.chatId!)!

    this.chat.messageSubject.subscribe((message) => {
      this.newMessage(message)
    })

    this.contactName = this.getLastMessageContact(this.chat).getName()

    this.numberMessagesWritten = this.chat.getMessages().length

    this.choices = this.getLastMessage(this.chat).getChoices();

  }

  public newMessage (message: Message) {
      this.numberMessagesWritten = this.chat.getMessages().length
      this.choices = this.getLastMessage(this.chat).getChoices();
  }

  public newChoice(choice: Choice) {
    this.chatService.addMessage(this.chat, {
      getContactId: () => 0,
      getText: choice.getText
    } as Message, true)
  }

  public getLastMessageContact(chat: Chat): any {
    return this.chatService.contactMap.get(
      chat.getMessages()[chat.getMessages().length - 1].getContactId()
    );
  }

  public getLastMessage(chat: Chat): any {
    return chat.getMessages()[chat.getMessages().length - 1];
  }



}
