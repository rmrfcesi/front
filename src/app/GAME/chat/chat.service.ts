import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { chats, messages, choices, contacts } from 'src/assets/scenario/scenario';
import { Chat, Choice, Contact, Message } from './model';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public chatMap: Map<Number, Chat> = new Map();

  public messageMap: Map<Number, Message> = new Map();

  public choicesMap: Map<Number, Choice> = new Map();

  public contactMap: Map<Number, Contact> = new Map();

  public static instance: ChatService;

  public newChat : Subject<Chat> = new Subject();


  constructor() {
    ChatService.instance = this;

    chats.forEach((chat) => {
      this.chatMap.set(chat.getId(), chat)
    });

    messages.forEach((message) => {
      this.messageMap.set(message.getId(), message)
    });

    choices.forEach((choice) => {
      this.choicesMap.set(choice.getId(), choice);
    });

    contacts.forEach((contact) => {
      this.contactMap.set(contact.getId(), contact);
    });

  }

  public addChat(chat: Chat) {
    console.log('addChat', chat);
    chat.getMessages().forEach((message) => {
      // this.addMessage(chat, message)
    })
    this.newChat.next(chat);
  }

  public makeChoice(chat: Chat, choice: Choice) {
    console.log('makeChoice')
    choice.execute(chat);
  }

  public addMessage(chat: Chat, message: Message, silent = false) {
    console.log('addMessage', chat, message);
    console.log(chat.getMessages())
    chat.getMessages().push(message);
    console.log(chat.getMessages())
    if(!silent){
      chat.messageSubject.next(message)
    }

  }

  public getMessageById (messageId: Number) : Message {
    console.log('getMessageById', messageId);
    return this.messageMap.get(messageId)!;
  }

  public getChatById (chatId: Number) : Chat {
    console.log('chatId', chatId);
    return this.chatMap.get(chatId)!;
  }

  public addMessageById(chat: Chat, messageId: Number) {
    console.log('sendMessageById', messageId);
    const message = this.getMessageById(messageId)
    this.addMessage(chat, message)
  }
  

}
