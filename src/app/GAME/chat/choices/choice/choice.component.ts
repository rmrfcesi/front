import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChatService } from '../../chat.service';
import { Chat, Choice } from '../../model';

@Component({
  selector: 'app-choice',
  templateUrl: './choice.component.html',
  styleUrls: ['./choice.component.scss']
})
export class ChoiceComponent implements OnInit {

  @Input()
  public chat! : Chat;

  @Input()
  public choices? : Choice[];

  @Output()
  public choice: EventEmitter<Choice> = new EventEmitter();

  constructor(private chartService : ChatService) { }

  ngOnInit(): void {

  }

  public handleChoiceClick(choice: Choice) {
    this.choice.emit(choice)
    this.chartService.makeChoice(this.chat, choice)
  }

}
