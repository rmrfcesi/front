import { Subject } from "rxjs";

export interface Identifiable {
    getId: () => number;
}

export interface Textable {
    getText: () => string;
}


export interface Chat extends Identifiable {
    getMessages: () => Message[];
    messageSubject: Subject<Message>;
}

export interface Message extends Identifiable, Textable {
    getContactId: () => number;
    getChoices: () => Choice[];
}


export interface Contact extends Identifiable {
    getName: () => string;
}


export interface Choice extends Identifiable, Textable {
    execute: (chat: Chat) => void;
}



