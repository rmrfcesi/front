import * as internal from "stream";

export interface Message {
    id_contact: number;
    content: string;
  }