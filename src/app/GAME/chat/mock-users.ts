import { Users } from "./users";
export const USERS: Users[] = [
    {
        id_contact: 1,
        name: 'Vincent' 
    },
    {
        id_contact: 2,
        name: 'Olivia' 
    },
    {
        id_contact: 3,
        name: 'Robert'
    },
    {
        id_contact: 4,
        name: 'Diane' 
    },
    {
        id_contact: 5,
        name: 'Léo' 
    },
    {
        id_contact: 6,
        name: 'Aldo' 
    },
]