import { ChatService } from 'src/app/GAME/chat/chat.service';
import { Choice, Message, Contact, Chat } from 'src/app/GAME/chat/model';

// First timeline
const chat1: Chat = {
  getId: () => 1,
  getMessages: () => [],
  getId: () => 1,
};

const contact: Contact = {
  getId: () => 2,
  getName: () => 'Mike Tyson',
};

const message1: Message = {
  getContactId: () => 2,
  getText: () => 'Hey, ça va? Je t’ai pas vu rentrer hier',
  getChoices: () => [choice1, choice2],
  getId: () => 101,
};
const choice1: Choice = {
  getText: () => 'Ouais, ça va tranquil',
  getId: () => 1,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 102);
    //ChatServive.sendMessageId(102);
  },
};
const choice2: Choice = {
  getText: () => 'Non, ça va pas, je me rappel de rien putain',
  getId: () => 2,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 103);
    //ChatServive.sendMessageId(103);
  },
};

const message2: Message = {
  getContactId: () => 2,
  getText: () => 'Je t’es vu hier avec la meuf, t’es rentré avec elle ?',
  getChoices: () => [choice4],
  getId: () => 102,
};

const message3: Message = {
  getContactId: () => 2,
  getText: () => 'Je t’es vu hier avec la meuf, peut-être elle saura',
  getChoices: () => [choice3],
  getId: () => 103,
};

const choice3: Choice = {
  getText: () => 'Je me rappel pas mec, j’ai pas son numéro',
  getId: () => 3,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 104);
    //ChatServive.sendMessageId(102);
  },
};
const choice4: Choice = {
  getText: () =>
    'Ouais. On a eu de bon temps hier. Mais j’ai oublié de prendre son numero',
  getId: () => 4,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 104);
    //ChatServive.sendMessageId(103);
  },
};

const message4: Message = {
  getContactId: () => 2,
  getText: () => "J'ai son numéro si tu veux",
  getChoices: () => [choice5],
  getId: () => 104,
};

const choice5: Choice = {
  getText: () => 'Ouais va y envoie le moi',
  getId: () => 5,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 202);
    //ChatServive.sendMessageId(103);
  },
};

// Second timeline
const chat2: Chat = {
  getId: () => 2,
  getMessages: () => [],
  getId: () => 2,
};

const contact3: Contact = {
  getId: () => 3,
  getName: () => 'Léa Portier',
};

const message5: Message = {
  getContactId: () => 3,
  getText: () => "Hey, torride la soirée d'hier, j'ai bien kiffé",
  getChoices: () => [choice6],
  getId: () => 202,
};

const choice6: Choice = {
  getText: () => 'Ouais super génial, j’étais sur le point de t’appeller',
  getId: () => 6,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 203);
    //ChatServive.sendMessageId(103);
  },
};
const message6: Message = {
  getContactId: () => 3,
  getText: () => 'Ouais, on se voit à l’école',
  getChoices: () => [choice7],
  getId: () => 203,
};

const choice7: Choice = {
  getText: () => 'Ok',
  getId: () => 7,
  execute: (chat1) => {
    //ChatServive.endChat();
  },
};

export const chats: Chat[] = [chat1, chat2];

export const messages: Message[] = [
  message1,
  message2,
  message3,
  message4,
  message5,
  message6,
];

export const choices: Choice[] = [
  choice1,
  choice2,
  choice3,
  choice4,
  choice5,
  choice7,
];

export const contacts: Contact[] = [contact, contact3];
