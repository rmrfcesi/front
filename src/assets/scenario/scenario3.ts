import { Choice, Message, Contact, Chat } from "src/app/GAME/chat/model";
// Timeline
const chat1: Chat = {
    getMessages: () => [message1],
    getId: () => 1,
};


const contact3: Contact = {
    getId: () => 3,
    getName: () => "Léa Portier"
}

const message1: Message = {
    getContactId: () => 3,
    getText: () => "Salut comment tu vas",
    getChoices: () => [choice1, choice2],
    getId : () => 101,
}    
const choice1: Choice = {
    getText: () => "Non pas trop",
    getId: () => 1,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 102);
        //ChatServive.sendMessageId(102);
    },
}
const choice2: Choice = {
    getText: () => "Oui ça va",
    getId: () => 2,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 103);
        //ChatServive.sendMessageId(103);
    },
}


const message2: Message = {
    getContactId: () => 3,
    getText: () => "Enfaite, je voulais te dire à ce sujet",
    getChoices: () => [choice3],
    getId : () => 102,
}    
const message3: Message = {
    getContactId: () => 3,
    getText: () => "Enfaite je voulais te parler de quelque chose",
    getChoices: () => [choice4],
    getId : () => 103,
}    

const choice3: Choice = {
    getText: () => "C’est quoi le souci",
    getId: () => 3,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 104);
        //ChatServive.sendMessageId(102);
    },
}
const choice4: Choice = {
    getText: () => "D’accord",
    getId: () => 4,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 104);
        //ChatServive.sendMessageId(103);
    },
}

const message4: Message = {
    getContactId: () => 3,
    getText: () => "J’ai fais un test recemment, j’ai le SIDA",
    getChoices: () => [choice5, choice6],
    getId : () => 104,
}  

const choice5: Choice = {
    getText: () => "Ah merde, tu veux en parler ?",
    getId: () => 5,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 105);
        //ChatServive.sendMessageId(102);
    },
}

const choice6: Choice = {
    getText: () => "D’accord, et alors?",
    getId: () => 6,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 106);
        //ChatServive.sendMessageId(102);
    },
}

const message5: Message = {
    getContactId: () => 3,
    getText: () => "Non t’inquiète, je vais bien, je te dis juste parceque je crois que tu devrais faire un test toi aussi",
    getChoices: () => [],
    getId : () => 105,
}  
const message6: Message = {
    getContactId: () => 3,
    getText: () => "Je pense juste que tu devrais faire un teste toi aussi enfoiré",
    getChoices: () => [],
    getId : () => 106,
}  



export const chats: Chat[] = [chat1];

export const messages: Message[] = [message1, message2, message3, message4, message5, message6];

export const choices: Choice[] = [choice1,choice2,choice3,choice4,choice5, choice6];

export const contacts: Contact[] = [contact3];