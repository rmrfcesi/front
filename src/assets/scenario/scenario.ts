import { Subject } from 'rxjs';
import { ChatService } from 'src/app/GAME/chat/chat.service';
import { Choice, Message, Contact, Chat } from 'src/app/GAME/chat/model';


const message1: Message = {
  getContactId: () => 1,
  getText: () => 'Hey ! T sur Grenoble ?',
  getChoices: () => [choice1],
  getId: () => 101,
};

// First timeline
const chat1: Chat = {
  messageSubject: new Subject(),
  getId: () => 1,
  messages: [message1],
  getMessages: () => (chat1 as any).messages,
} as any;

const contact: Contact = {
  getId: () => 1,
  getName: () => 'John Doe',
};
const choice1: Choice = {
  getText: () => 'Ouaip',
  getId: () => 1,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 102);
    // ChatService.instance.addChat(chat2)
    //ChatServive.sendMessageId(102);
  },
};

const message2: Message = {
  getContactId: () => 1,
  getText: () => 'Ya une soirée demain ça te chauffe ?',
  getChoices: () => [choice2, choice3],
  getId: () => 102,
};

const choice2: Choice = {
  getText: () => 'Grave !!',
  getId: () => 2,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 104);
    //ChatServive.sendMessageId(104);
  },
};

const choice3: Choice = {
  getText: () => 'Nope pas pour moi',
  getId: () => 3,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 103);
    //ChatServive.sendMessageId(103);
  },
};

const message3: Message = {
  getContactId: () => 1,
  getText: () =>
    'Oh aller faut sortir des fois, yaura des meufs en plus mec !!',
  getChoices: () => [choice4],
  getId: () => 103,
};

const choice4: Choice = {
  getText: () => 'Pff aller d’accord, mais on rentre pas trop tard',
  getId: () => 4,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 104);
    //ChatServive.sendMessageId(104);
  },
};

const message4: Message = {
  getContactId: () => 1,
  getText: () => 'Nikeeel\nElle sera là la nouvelle trop chaude',
  getChoices: () => [choice5],
  getId: () => 104,
};

const choice5: Choice = {
  getText: () => 'Elle est où la soirée ?',
  getId: () => 5,
  execute: (chat1) => {
    ChatService.instance.addMessageById(chat1, 105);
    //ChatServive.sendMessageId(105);
  },
};

const message5: Message = {
  getContactId: () => 1,
  getText: () => 'Ah mec t’abuse\nC’est au local de l’équipe de foot',
  getChoices: () => [choice13],
  getId: () => 105,
};

//Scène parallèle

const chat2: Chat = {
  messageSubject: new Subject(),
  getId: () => 2,
  messages: [message1],
  getMessages: () => (chat1 as any).messages,
} as any;

const contact1: Contact = {
  getId: () => 2,
  getName: () => 'Mike Tyson',
};

const message6: Message = {
  getContactId: () => 2,
  getText: () => "C'etait chiant cet aprem!!",
  getChoices: () => [choice7, choice8],
  getId: () => 201,
};
const choice7: Choice = {
  getText: () => 'Graaave',
  getId: () => 7,
  execute: (chat2) => {
    ChatService.instance.addMessageById(chat2, 202);
    //ChatServive.sendMessageId(202);
  },
};

const choice8: Choice = {
  getText: () => 'En vrai ça va',
  getId: () => 8,
  execute: (chat2) => {
    ChatService.instance.addMessageById(chat2, 202);
    //ChatServive.sendMessageId(202);
  },
};

const message7: Message = {
  getContactId: () => 2,
  getText: () => 'Par contre ta vu Gab comment il était mal ?',
  getChoices: () => [choice9],
  getId: () => 202,
};

const choice9: Choice = {
  getText: () => 'Ouai de fou',
  getId: () => 9,
  execute: (chat2) => {
    ChatService.instance.addMessageById(chat2, 203);
    //ChatServive.sendMessageId(203);
  },
};

const message8: Message = {
  getContactId: () => 2,
  getText: () =>
    'Mdr, il est tout le temps défoncé ce mec.\nDailleurs tu viens a la soirée au stade ?',
  getChoices: () => [choice2, choice3],
  getId: () => 203,
};

const choice10: Choice = {
  getText: () => 'Nan mec trop la flemme',
  getId: () => 10,
  execute: (chat2) => {
    ChatService.instance.addMessageById(chat2, 204);
    //ChatServive.sendMessageId(204);
  },
};

const choice11: Choice = {
  getText: () => 'Aller !',
  getId: () => 11,
  execute: (chat2) => {
    ChatService.instance.addMessageById(chat2, 205);
    //ChatServive.sendMessageId(205);
  },
};

const message9: Message = {
  getContactId: () => 2,
  getText: () => 'Gros faut sortir des fois',
  getChoices: () => [choice12],
  getId: () => 204,
};

const choice12: Choice = {
  getText: () => 'Ouai aller ok pour cte fois',
  getId: () => 12,
  execute: (chat2) => {
    ChatService.instance.addMessageById(chat2, 205);
    //ChatServive.sendMessageId(205);
  },
};

const message10: Message = {
  getContactId: () => 2,
  getText: () => 'Super, a toute alors !',
  getChoices: () => [choice13],
  getId: () => 205,
};

// Final choice
const choice13: Choice = {
  getText: () => 'Aller à la soirée',
  getId: () => 13,
  execute: (chat2) => {
    //ChatServive.endChat();
  },
};

export const chats: Chat[] = [chat1, chat2];

export const messages: Message[] = [
  message1,
  message2,
  message3,
  message4,
  message5,
  message6,
  message7,
  message8,
  message9,
  message10,
];

export const choices: Choice[] = [
  choice1,
  choice2,
  choice3,
  choice4,
  choice5,
  choice7,
  choice8,
  choice9,
  choice10,
  choice11,
  choice12,
  choice13,
];

export const contacts: Contact[] = [contact, contact1];
