import { Choice, Message, Contact, Chat } from "src/app/GAME/chat/model";
// Timeline
const chat1: Chat = {
    getMessages: () => [],
    getId: () => 1,
};


const contact4: Contact = {
    getId: () => 4,
    getName: () => "Créature mistique"
}



const message1: Message = {
    getContactId: () => 4,
    getText: () => "Gogo Attack queue de fer",
    getChoices: () => [choice1],
    getId : () => 101,
}    
const choice1: Choice = {
    getText: () => "Parer avec bulle d’eau",
    getId: () => 1,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 102);
        //ChatServive.sendMessageId(102);
    },
}

const message2: Message = {
    getContactId: () => 4,
    getText: () => "Block le avec Ancrage wisticrabe",
    getChoices: () => [choice2],
    getId : () => 102,
}    
const choice2: Choice = {
    getText: () => "Ball’Ombre Chimpanfeu",
    getId: () => 2,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 103);
        //ChatServive.sendMessageId(102);
    },
}

const message3: Message = {
    getContactId: () => 4,
    getText: () => "Esquive !",
    getChoices: () => [choice3,choice4],
    getId : () => 103,
}    

const choice3: Choice = {
    getText: () => "Poursuit le mais n’attaque pas !",
    getId: () => 3,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 103);
        //ChatServive.sendMessageId(102);
    },
}
const choice4: Choice = {
    getText: () => "Poursuit le et attaque !",
    getId: () => 4,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 103);
        //ChatServive.sendMessageId(102);
    },
}

const message4: Message = {
    getContactId: () => 4,
    getText: () => "HP --",
    getChoices: () => [choice5, choice6],
    getId : () => 104,
}    

const message5: Message = {
    getContactId: () => 4,
    getText: () => "Il se fou de notre guele l’enfoiré !",
    getChoices: () => [choice7],
    getId : () => 105,
}  


const choice5: Choice = {
    getText: () => "Mewtoo je te choisi",
    getId: () => 5,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 106);
        //ChatServive.sendMessageId(102);
    },
}

const message6: Message = {
    getContactId: () => 4,
    getText: () => "Mewtoo a choppé le papillonavirus !?!",
    getChoices: () => [],
    getId : () => 106,
}  

const choice6: Choice = {
    getText: () => "Bon bruh shui à cours de pokemon",
    getId: () => 6,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 107);
        //ChatServive.sendMessageId(102);
    },
}

const message7: Message = {
    getContactId: () => 4,
    getText: () => "Adios mon poto",
    getChoices: () => [],
    getId : () => 107,
}  


const choice7: Choice = {
    getText: () => "On foonnnce",
    getId: () => 7,
    execute: (chat1) => {
        ChatService.instance.addMessageById(chat1, 108);
        //ChatServive.sendMessageId(102);
    },
}

const message8: Message = {
    getContactId: () => 4,
    getText: () => "Ah merde, mon pokemon a choppé le SIDA",
    getChoices: () => [],
    getId : () => 108,
}  


export const chats: Chat[] = [chat1];

export const messages: Message[] = [message1, message2, message3, message4, message5, message6, message7, message8];

export const choices: Choice[] = [choice1,choice2,choice3,choice4,choice5, choice6, choice7];

export const contacts: Contact[] = [contact4];