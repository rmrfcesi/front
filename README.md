# Comment avons nous travaillé
Tout au long de notre développement nous avons fonctionné avec des branches et des merges requests.

Une CI/CD nous a permis de redéployer automatiquement sur notre serveur de prod à chaque merge sur la branche main.

Serveur de prod : [http://x.terribleupti.me/](http://x.terribleupti.me/ "http://x.terribleupti.me/")

# Fonctionnement de notre application

Notre application se lance directement sur le jeu. Le jeu consiste à échanger des messages avec des amis. Des scénarios sont écrits et permettent d'en venir à parler des IST et d'en apprendre sur ces maladies en ce mettant dans la peau d'un jeune inconscient.

Ensuite l'application comporte plusieurs onglets permettant de se renseigner sur les IST, des témoignages et de poser des questions dans une FAQ